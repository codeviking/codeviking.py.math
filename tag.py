#!/usr/bin/env python3
"""
This script sets the hg tag equal to the current version.
"""
import subprocess

exec(open('src/codeviking/math/_version.py').read())
print(__version__)
cmd = ['git', 'tag', __version__]
subprocess.call(cmd)
