#!/bin/sh
# Note that docs can't be built unless ALL codeviking.* packages are uninstalled - TODO - figure out
# how to work around this.
cd docs
make html
cd ..
python3 setup.py sdist
twine upload dist/*

