*****************************
CodeViking Math Documentation
*****************************

:mod:`codeviking.math`
*****************************

A collection of mathematical utility funcitons.

This package currently supports Python 3 only.  There is no planned support for
Python 2.  Patches that provide Python 2 compatibility are welcome.

**WARNING**:  Code and documentation are currently in beta testing.
Everything documented here is expected to work, but not all features have
been thoroughly tested yet.  If you find any bugs, please file a bug report.
at https://bitbucket.org/codeviking/python-codeviking.math/


.. toctree::
    :maxdepth: 8

    comparisons
    ease
    functions
    geom2d
    linalg
    primitives


:mod:`codeviking.math`
*****************************


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

