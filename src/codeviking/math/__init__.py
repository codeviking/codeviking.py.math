from .comparisons import *


def sign(x):
    if x < 0:
        return -1
    if 0 < x:
        return 1
    return 0



