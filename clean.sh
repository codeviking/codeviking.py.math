#!/bin/sh
find ./  -name __pycache__ -delete
find ./  -name '*.pyc' -delete
find ./ -name '*.kate-swp' -delete
find ./ -name '*~' -delete
rm -rf CodeViking.math.egg-info
rm -rf build dist
cd docs
make clean
cd ..
