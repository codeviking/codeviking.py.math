codeviking.math README
===========================

Math utility functions.



Installation
------------

    pip install codeviking.math

Documentation
-------------

    http://codeviking-math.readthedocs.org/

Support
-------

See the project home page at
https://bitbucket.org/codeviking/python-codeviking.math/
to file a bug report or request a feature.
